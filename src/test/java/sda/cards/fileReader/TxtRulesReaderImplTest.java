package sda.cards.fileReader;

import org.junit.Test;
import sda.cards.issuersRules.Rule;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TxtRulesReaderImplTest {

    private final TxtRulesReaderImpl rulesReader = new TxtRulesReaderImpl
            ("C:\\Users\\User\\IdeaProjects\\sda-intermediate\\src\\main\\resources\\CardIssuersList.txt");

    @Test
    public void readRules() throws Exception {
        Rule expected = new Rule(12345678, 16, "SDA_test");
        List<Rule> rules = rulesReader.buildRules();
        Rule rule = rules.get(0);
        // need to override 'equals' method for Rule object
        assertEquals(expected, rule);
    }

    @Test
    public void should_convert_list_of_strings_to_list_of_rules() {
        List<String> strings = new ArrayList<>();
        strings.add("12345678;16;SDA_test");
        strings.add("4;16;Visa");
        strings.add("51-55;16;Master Card");
        strings.add("34,37;15;American Express");
        List<Rule> rules = rulesReader.splitLinesIntoRules(strings);
        SeedRulesBuilder rulesBuilder = new SeedRulesBuilder();
        List<Rule> expected = rulesBuilder.buildRules();
        assertEquals(expected, rules);
    }

    @Test
    public void split_and_parse_String_List_of_Integers() throws Exception {
        List<Integer> prefixes = TxtRulesReaderImpl.splitPrefixes("5-7,28-30,54,78,90-91");
        List<Integer> integers = new ArrayList<>(10);
        integers.add(5);
        integers.add(6);
        integers.add(7);
        integers.add(28);
        integers.add(29);
        integers.add(30);
        integers.add(54);
        integers.add(78);
        integers.add(90);
        integers.add(91);

        assertEquals(integers, prefixes);
    }
}