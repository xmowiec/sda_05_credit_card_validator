package sda.cards;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import sda.cards.validate.IValidatorAlgorithm;
import sda.cards.validate.ValidatorLuhnAlgorithmImpl;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ValidatorLuhnAlgorithmImplTest {

    // create new IValidatorAlgorithm object
    private IValidatorAlgorithm validator = new ValidatorLuhnAlgorithmImpl();
    private String cardNo;
    private boolean isValid;

    public ValidatorLuhnAlgorithmImplTest(String cardNo, boolean isValid) {
        this.cardNo = cardNo;
        this.isValid = isValid;
    }

    @Parameters
    public static Collection<Object[]> setData() {
        return Arrays.asList(new Object[][]{
                {"4977807180348236", true},
                {"4532735407971521", false},
                {"5518603849293449", true},
                {"4446326792841535", false}
        });
    }

    @Test
    public void validatorTest() {
        boolean result = validator.validate(this.cardNo);
        assertEquals(this.isValid, result);
    }
}