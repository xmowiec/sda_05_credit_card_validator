package sda.cards;

import sda.cards.issuersRules.IIssuerDetector;
import sda.cards.issuersRules.IssueDetectorImpl;
import sda.cards.validate.IValidatorAlgorithm;
import sda.cards.validate.ValidatorLuhnAlgorithmImpl;

import java.io.IOException;

public class SDACardValidator implements ICardValidator {

    /**
     * Path to file contains base of issuer's rules
     */
    private final String ruleBaseFilePath;

    /**
     * Constructor with path to file with rule base
     * Suppose it's necessary to create new validator
     * @param ruleBaseFilePath path to file with rule base
     */
    public SDACardValidator(String ruleBaseFilePath) {
        this.ruleBaseFilePath = ruleBaseFilePath;
    }

    @Override
    public ValidationResult validateCardNo(String cardNo) throws IOException {

        // 1. step - validate card number if it's correct with Luhn algorithm
        IValidatorAlgorithm validatorAlgorithm = new ValidatorLuhnAlgorithmImpl();
        boolean isValid = validatorAlgorithm.validate(cardNo);

        // 2. step - detect the issuer of card with rules get from file
        IIssuerDetector issuerDetector = new IssueDetectorImpl(ruleBaseFilePath);
        String issuer = issuerDetector.detectIssuer(cardNo);

        return new ValidationResult(issuer, isValid);
    }
}
