package sda.cards;

import java.io.IOException;

public interface ICardValidator {

    /**
     * Our own card number validator (check correctness of number and get issuer)
     *
     * @param cardNo card number to validate
     * @return ValidationResult type
     */
    ValidationResult validateCardNo (String cardNo) throws IOException;
}
