package sda.cards;

public class ValidationResult {

    private final String issuer;
    private final Boolean isValid;

    public ValidationResult(String issuer, Boolean isValid) {
        this.issuer = issuer;
        this.isValid = isValid;
    }

    public String getIssuer() {
        return issuer;
    }

    public Boolean getIsValid() {
        return isValid;
    }

}
