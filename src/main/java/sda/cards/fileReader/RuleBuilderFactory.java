package sda.cards.fileReader;

import sda.cards.issuersRules.Rule;

import java.io.IOException;
import java.util.List;

public class RuleBuilderFactory {

    /**
     * use pattern Factory
     *
     * @param ruleBaseFilePath path to file with rules
     * @return list of rules
     */
    public static List<Rule> produceRules(String ruleBaseFilePath) throws IOException {
        IRuleReader rulesReader = null;

        if (ruleBaseFilePath != null && !ruleBaseFilePath.isEmpty()) {
            if (ruleBaseFilePath.endsWith(".txt")) {
                rulesReader = new TxtRulesReaderImpl(ruleBaseFilePath);
            } else if (ruleBaseFilePath.endsWith(".csv")) {
                rulesReader = new CsvRulesReaderImpl(ruleBaseFilePath);
            }
        } else {
            rulesReader = new SeedRulesBuilder(); // default 'seed' rules builder
        }
        return rulesReader.buildRules();
    }
}
