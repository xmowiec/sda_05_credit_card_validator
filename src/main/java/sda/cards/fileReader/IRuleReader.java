package sda.cards.fileReader;

import sda.cards.issuersRules.Rule;

import java.io.IOException;
import java.util.List;

public interface IRuleReader {

    /**
     * Read issuer rules from file
     * @return list of issuers rules
     * @throws IOException when file does not exist
     */
    List<Rule> buildRules() throws IOException;
}
