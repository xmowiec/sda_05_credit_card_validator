package sda.cards.fileReader;

import sda.cards.issuersRules.Rule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TxtRulesReaderImpl extends AbstractRulesReader {

    private static final String COMMENT = "#";
    private static final String REGEX_RULE_FIELDS = ";";
    private static final String REGEX_PREFIXES = ",";
    private static final String REGEX_PREFIXES_RANGE = "-";

    public TxtRulesReaderImpl(String ruleBaseFilePath) {
        super(ruleBaseFilePath);
    }

    @Override
    public List<Rule> buildRules() throws IOException {

        // 1. step - read rules as Strings from file
        List<String> lines = readRules(pathToFile);
        // 2. build rules from list of Strings
        return splitLinesIntoRules(lines);
    }

    private List<String> readRules(String ruleBaseFilePath) throws IOException {
        return Files.readAllLines(Paths.get(ruleBaseFilePath));
    }

    List<Rule> splitLinesIntoRules(List<String> lines) {
        List<Rule> rules = new ArrayList<>();
        for (String line : lines) {
            if (line.startsWith(COMMENT)) {
                continue;
            }
            // split each line with separator ';'
            String[] split = line.split(REGEX_RULE_FIELDS);

            // line format: list_of_prefixes;length;issuer
//                int prefix = Integer.parseInt(split[0]);
            List<Integer> prefixes = splitPrefixes(split[0]);
            int length = Integer.parseInt(split[1]);
            String issuer = split[2];

            // add all the rules from one line of file to list of rules
            for (Integer prefix : prefixes) {
                rules.add(new Rule(prefix, length, issuer));
            }

        }
        return rules;
    }

    /**
     * Split and parse String to list of Integers
     * access modifier 'package-private' for testing purposes
     *
     * @param sprefixes format eg. 5-7,28-30,54,78,90-91
     * @return list of Integers (eg. 5, 6, 7, 28, 29, 30, 54, 78, 90, 91)
     */
    static List<Integer> splitPrefixes(String sprefixes) {
        List<Integer> prefixes = new ArrayList<>();
        String[] split = sprefixes.split(REGEX_PREFIXES);
        for (String s : split) {
            String[] t = s.split(REGEX_PREFIXES_RANGE);
            if (t.length == 1) {
                prefixes.add(Integer.parseInt(t[0]));
            } else {
                for (int i = Integer.parseInt(t[0]); i <= Integer.parseInt(t[1]); i++) {
                    prefixes.add(i);
                }
            }
        }
        return prefixes;
    }
}
