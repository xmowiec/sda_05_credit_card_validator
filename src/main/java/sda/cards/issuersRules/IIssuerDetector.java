package sda.cards.issuersRules;

import java.io.IOException;

public interface IIssuerDetector {

    /**
     * Detect issuer of card number, without specify any rule file
     * @param cardNo card number
     * @return issuer of card
     * @throws IOException when file not found
     */
    String detectIssuer(String cardNo) throws IOException;

}
