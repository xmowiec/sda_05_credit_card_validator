package sda.cards.issuersRules;

import sda.cards.fileReader.RuleBuilderFactory;

import java.io.IOException;
import java.util.List;

public class IssueDetectorImpl implements IIssuerDetector {

    private final String ruleBaseFilePath;

    public IssueDetectorImpl(String ruleBaseFilePath) {
        this.ruleBaseFilePath = ruleBaseFilePath;
    }

    public IssueDetectorImpl() {
        this(null);
    }

    @Override
    public String detectIssuer(String cardNo) throws IOException {
        // use Factory pattern
        // use Singleton pattern (only one instance)
        RulesRepository.getInstance().addRules(RuleBuilderFactory.produceRules(ruleBaseFilePath));
        List<Rule> rules = RulesRepository.getInstance().getAllRules();

        // compare cardNo with rules from list
        for (Rule rule : rules) {
            if (cardNo.startsWith(String.valueOf(rule.getPrefix())) && cardNo.length() == rule.getLength()) {
                return rule.getIssuer();
            }
        }
        return null;
    }
}
