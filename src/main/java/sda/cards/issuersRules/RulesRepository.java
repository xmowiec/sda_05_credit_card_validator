package sda.cards.issuersRules;

import java.util.ArrayList;
import java.util.List;

public class RulesRepository {

    /**
     * Empty, private constructor to deny create more objects
     */
    private RulesRepository() {
    }

    /**
     * Singleton pattern - create new object (only once)
     */
    private static RulesRepository instance = new RulesRepository();

    public static RulesRepository getInstance() {
        return instance;
    }

    private List<Rule> rules = new ArrayList<>();

    public List<Rule> getAllRules() {
        return this.rules;
    }

    public void addRules(List<Rule> rules) {
        this.rules.addAll(rules);
    }
}
